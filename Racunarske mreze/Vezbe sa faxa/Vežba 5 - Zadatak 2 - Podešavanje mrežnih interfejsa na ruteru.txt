Kliknite na ruter R1 - tab CLI - enter
ukucajte lozinku cisco 

Trenutno ste u EXEC mode-u - R1>
Ukucajte komandu enable
R1>enable
ukucajte lozinku class

Sada ste u Privileged EXEC mode-u - R1r#
Za pregled svih interfejsa
R1#show interfaces
Za pregled samo serijskog interfejsa Serial 0/0/0
R1#show interface Serial 0/0/0

Da biste pregledali sve adrese na mre�nim interfejsima
R1#show ip interface brief

ukucajte komandu configure terminal
R1#configure terminal

Sada ste u Global configuration mode-u - R1(config)#
obratite pa�nju na naredne komande i na mesta na kojima se nalazite
da se vratite na Global configuration mode potrebno je otkucati exit

Pristupite pode�avanju mre�nog interfejsa GigabitEthernet 0/0
R1(config)#interface GigabitEthernet 0/0

Dodelite IP adresu mre�nom interfejsu
R1(config-if)#ip address 192.168.10.1 255.255.255.0

Aktivirajte mre�ni interfejs
R1(config-if)#no shutdown

Podesite opis mre�nog interfejsa
R1(config-if)#description LAN connection to S1
R1(config-if)#exit

Podesite i ostale mre�ne interfejse na R1 i R2 po istom principu
R1(config)#interface GigabitEthernet 0/1
R1(config-if)#ip address 192.168.11.1 255.255.255.0
R1(config-if)#no shutdown
R1(config-if)#description LAN connection to S2
R1(config-if)#exit
R1(config)#exit

R2(config)#interface GigabitEthernet 0/0
R2(config-if)#ip address 10.1.1.1 255.255.255.0
R2(config-if)#no shutdown
R2(config-if)#description LAN connection to S3
R2(config-if)#exit

R2(config)#interface GigabitEthernet 0/1
R2(config-if)#ip address 10.1.2.1 255.255.255.0
R2(config-if)#no shutdown
R2(config-if)#description LAN connection to S4
R2(config-if)#exit
R2(config)#exit

Iskopirajte na oba rutera trenutnu konfiguraciju u startnu konfiguraciju.
R1#copy running-config startup-config
R2#copy running-config startup-config