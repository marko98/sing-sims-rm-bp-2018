﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace sql1
{
    class Program
    {
        static void Main(string[] args)
        {
            //test();

            string connetionString = "Data Source=localhost;Initial Catalog=vezbe04;User ID=root;Password=";
            MySqlConnection conn = new MySqlConnection(connetionString);
            conn.Open();

            MySqlCommand command = conn.CreateCommand();

            createTables(command);
            //fillTables(command);
            executeQueries(command);
            Console.ReadKey();

            conn.Close();
        }

        private static void executeQueries(MySqlCommand command)
        {
            command.CommandText = "SELECT kupci.ime, COUNT(*) AS broj_porudzbina "
                                + "FROM porudzbine "
                                + "INNER JOIN kupci ON kupci.id = porudzbine.id_kupca "
                                + "GROUP BY kupci.ime "
                                + "ORDER BY broj_porudzbina DESC "
                                + "LIMIT 1; "

                                + "SELECT artikli.ime, SUM(porudzbine.kolicina) AS ukupno_poruceno "
                                + "FROM porudzbine "
                                + "INNER JOIN artikli on artikli.id = porudzbine.id_artikla "
                                + "INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice "
                                + "WHERE prodavnice.grad = 'Beograd' "
                                + "GROUP BY artikli.ime "
                                + "ORDER BY ukupno_poruceno DESC "
                                + "LIMIT 1; "

                                + "SELECT kupci.ime, porudzbine.id as id_porudzbine, porudzbine.kolicina* artikli.cena as ukupna_cena "
                                + "FROM porudzbine "
                                + "INNER JOIN kupci on kupci.id = porudzbine.id_kupca "
                                + "INNER JOIN artikli on artikli.id = porudzbine.id_artikla; "

                                + "SELECT prodavnice.ime, SUM(porudzbine.kolicina * artikli.cena) as zarada "
                                + "FROM porudzbine "
                                + "INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice "
                                + "INNER JOIN artikli on artikli.id = porudzbine.id_artikla "
                                + "GROUP BY prodavnice.ime; "

                                + "SELECT * "
                                + "FROM porudzbine "
                                + "INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice "
                                + "WHERE prodavnice.grad = 'Novi Sad'; "

                                + "SELECT prodavnice.grad, COUNT(*) AS broj_porudzbina "
                                + "FROM porudzbine "
                                + "INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice "
                                + "GROUP BY prodavnice.grad; "

                                + "SELECT kupci.ime, AVG(porudzbine.kolicina * artikli.cena) AS prosecna_cena_po_porudzbini "
                                + "FROM porudzbine "
                                + "INNER JOIN kupci on kupci.id = porudzbine.id_kupca "
                                + "INNER JOIN artikli on artikli.id = porudzbine.id_artikla "
                                + "GROUP BY kupci.ime "
                                + "ORDER BY prosecna_cena_po_porudzbini DESC; "

                                + "SELECT porudzbine.datum, COUNT(*) AS broj_porudzbina "
                                + "FROM porudzbine "
                                + "GROUP BY porudzbine.datum; "


                                + "SELECT *, porudzbine.kolicina* artikli.cena AS cena_porudzbine "
                                + "FROM porudzbine "
                                + "INNER JOIN artikli on artikli.id = porudzbine.id_artikla "
                                + "ORDER BY cena_porudzbine DESC "
                                + "LIMIT 1; ";
            System.Data.DataSet ubb = executeSelect(command);   

            for (int k = 0; k < ubb.Tables.Count; k++) {
                string tabela = "Tabela" + k + ": \n";
                string naziv_kolona = "";
                string redovi = "";
                for (int i = 0; i < ubb.Tables[k].Rows.Count; i++) { 
                    string red = "";
                    for (int j = 0; j < ubb.Tables[k].Columns.Count; j++) {
                        if (i == 0) {
                            naziv_kolona += ubb.Tables[k].Columns[j].ColumnName + " | ";
                        }
                        red += ubb.Tables[k].Rows[i][j] + ", ";
                    }
                    //Console.WriteLine(red);
                    redovi += red + "\n";
                }
                Console.WriteLine(tabela + naziv_kolona + "\n" + redovi + "\n");
            }

            /*for (int podaci = 0; podaci < ubb.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb.Tables[0].Rows[podaci]["Ime"] + " " + ubb.Tables[0].Rows[podaci]["prezime"]);
            }
            Console.WriteLine("");

            command.CommandText = "SELECT Ime,Prezime FROM profesori WHERE BrProf='1001'";
            System.Data.DataSet ubb2 = executeSelect(command);

            for (int podaci = 0; podaci < ubb2.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb2.Tables[0].Rows[podaci]["Ime"] + " " + ubb2.Tables[0].Rows[podaci]["prezime"]);
            }

            Console.WriteLine("");
            command.CommandText = "SELECT BrPredmeta,Predmeti.ime FROM predmeti,profesori WHERE Prezime='Stojmenovic' AND Profesori.BrProf=predmeti.BrProf";
            System.Data.DataSet ubb3 = executeSelect(command);

            for (int podaci = 0; podaci < ubb3.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb3.Tables[0].Rows[podaci]["Ime"] + " " + ubb3.Tables[0].Rows[podaci]["BrPredmeta"]);
            }

            Console.WriteLine("");
            command.CommandText = "SELECT profesori.Ime,profesori.Prezime FROM predmeti,profesori,studenti,upis WHERE Profesori.BrProf = Predmeti.BrProf AND Predmeti.BrPredmeta=Upis.BrPredmeta AND Upis.BrInd=Studenti.BrInd AND Studenti.Ime='Nada' AND Studenti.Prezime='Petrovic'";
            ubb3 = executeSelect(command);

            for (int podaci = 0; podaci < ubb3.Tables[0].Rows.Count; ++podaci) //za svaki red
            {
                Console.WriteLine(ubb3.Tables[0].Rows[podaci]["Ime"] + " " + ubb3.Tables[0].Rows[podaci]["Prezime"]);
            }
            Console.WriteLine("");*/
        }

        private static void fillTables(MySqlCommand command)
        {
            command.CommandText = "INSERT INTO kupci VALUES(0,'Petar', 'Cacak'), "
                                + "(0, 'Marko', 'Beograd'), "
                                + "(0, 'Ivan', 'Cacak'), "
                                + "(0, 'Dusan', 'Beograd'), "
                                + "(0, 'Jelena', 'Novi Sad'), "
                                + "(0, 'Ana', 'Novi Sad'), "
                                + "(0, 'Stefan', 'Novi Sad')";
            executeNonQuery(command);

            command.CommandText = "INSERT INTO artikli VALUES(0,'hleb', 50), "
                                + "(0,'mleko', 100), "
                                + "(0,'jaja', 5), "
                                + "(0,'sok', 200), "
                                + "(0,'voda', 50)";
            executeNonQuery(command);

            command.CommandText = "INSERT INTO prodavnice VALUES(0,'Maxi-Ca', 'Cacak'), "
                                + "(0,'Idea-Bg', 'Beograd'), "
                                + "(0,'Merkator', 'Beograd'), "
                                + "(0,'Maxi-NS', 'Novi Sad'), "
                                + "(0,'Idea-NS', 'Novi Sad')";
            executeNonQuery(command);

            command.CommandText = "INSERT INTO porudzbine VALUES(0,1, 1, 1, 5, '2016-1-1'), "
                                + "(0,2, 1, 2, 2, '2017-2-1'), "
                                + "(0,2, 1, 3, 3, '2017-2-1'), "
                                + "(0,3, 3, 4, 1, '2018-6-1'), "
                                + "(0,3, 4, 5, 1, '2015-7-1'), "
                                + "(0,3, 5, 1, 2, '2016-9-1'), "
                                + "(0,4, 3, 3, 10, '2016-2-5'), "
                                + "(0,4, 5, 4, 2, '2018-6-1'), "
                                + "(0,5, 1, 5, 4, '2017-4-3'), "
                                + "(0,7, 3, 4, 7, '2016-9-1'), "
                                + "(0,7, 5, 2, 2, '2015-7-1')";
            executeNonQuery(command);
        }

        private static void createTables(MySqlCommand command)
        {
            command.CommandText = "CREATE TABLE IF NOT EXISTS kupci ("
                            + "id           INT NOT NULL AUTO_INCREMENT, "
                            + "ime          VARCHAR(255) NOT NULL, "
                            + "grad         VARCHAR(255) NOT NULL, "
                            + "PRIMARY KEY (id))";
            executeNonQuery(command); //command.ExecuteNonQuery();

            command.CommandText = "CREATE TABLE IF NOT EXISTS artikli ("
                            + "id           INT NOT NULL AUTO_INCREMENT, "
                            + "ime          VARCHAR(255) NOT NULL, "
                            + "cena         INT NOT NULL, "
                            + "PRIMARY KEY (id))";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS prodavnice ("
                            + "id           INT NOT NULL AUTO_INCREMENT, "
                            + "ime          VARCHAR(255) NOT NULL, "
                            + "grad         VARCHAR(255) NOT NULL, "
                            + "PRIMARY KEY (id))";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS porudzbine ("
                            + "id             INT NOT NULL AUTO_INCREMENT, "
                            + "id_kupca       INT NOT NULL, "
                            + "id_artikla     INT NOT NULL, "
                            + "id_prodavnice  INT NOT NULL, "
                            + "kolicina       INT NOT NULL, "
                            + "datum          DATE NOT NULL, "
                            + "PRIMARY KEY (id), "
                            + "FOREIGN KEY(id_kupca) REFERENCES kupci(id), "
                            + "FOREIGN KEY(id_artikla) REFERENCES artikli(id), "
                            + "FOREIGN KEY(id_prodavnice) REFERENCES prodavnice(id))";
            executeNonQuery(command);
        }

        private static void test()
        {
            List<string> asdf = new List<string>();

            asdf.Add("marko");
            asdf.Add("marina");
            asdf.Any(o => o == "asdf");
            var mar = asdf.Where(f => f.Substring(0, 3) == "mar");

            foreach (string value in mar)
            {
                Console.WriteLine(value);
            }
        }

        public static string dtfordb(DateTime dt)
        {
            return dt.Year + "-" + dt.Month + "-" + dt.Day + " " + dt.TimeOfDay;
        }

        public static void executeNonQuery(MySqlCommand commandsw)
        {
            bool b = false;

            while (!b)
            {
                try
                {
                    if (!commandsw.Connection.Ping() || commandsw.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        commandsw.Connection.Open();
                    }
                    commandsw.ExecuteNonQuery();
                    b = true;
                }
                catch
                {
                    Console.WriteLine(DateTime.Now + "Vrtimo se u petlji upisa u bazu");
                    Console.WriteLine(DateTime.Now + " " + commandsw.CommandText);
                    b = false;
                }
            }
        }

        public static System.Data.DataSet executeSelect(MySqlCommand command)
        {
            bool b = false;
            System.Data.DataSet cas = new System.Data.DataSet();

            while (!b)
            {
                try
                {
                    if (!command.Connection.Ping() || command.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        command.Connection.Open();
                    }
                    MySqlDataAdapter adapter = new MySqlDataAdapter(command.CommandText, command.Connection);
                    adapter.Fill(cas);
                    b = true;
                }
                catch
                {
                    Console.WriteLine(DateTime.Now + "Vrtimo se u petlji SELECT u bazi");
                    Console.WriteLine(DateTime.Now + " " + command.CommandText);
                    b = false;
                }
            }
            return cas;
        }
    }
}
