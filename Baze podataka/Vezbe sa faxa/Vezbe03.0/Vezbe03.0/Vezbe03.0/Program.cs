﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Vezbe03._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Do you want to create tables: (Y/N)");
            string answer_base = Console.ReadLine();
            if (answer_base.ToLower() == "y")
            {
                createTables();
            }

            Console.WriteLine("Do you want to insert data: (Y/N)");
            string fill_base = Console.ReadLine();
            if (fill_base.ToLower() == "y")
            {
                insertData();
            }

            Console.WriteLine("Do you want to execute some queries: (Y/N)");
            string execute_queries = Console.ReadLine();
            if (execute_queries.ToLower() == "y")
            {
                executeQueries();
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        private static void createTables() {
            //implementacija metode

            string query = "CREATE TABLE gosti( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "prezime VARCHAR(255) NOT NULL, " +
                "dob INT NOT NULL, " +
                "kreditna_kartica VARCHAR(255) NOT NULL, " +
                "PRIMARY KEY(id));" +

                "CREATE TABLE sobe( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "vrsta_sobe VARCHAR(255) NOT NULL, " +
                "cena INT NOT NULL, " +
                "broj_lezaja INT NOT NULL, " +
                "gost_id INT, " +
                "PRIMARY KEY(id), " +
                "FOREIGN KEY (gost_id) REFERENCES gosti(id));" +

                "CREATE TABLE rezervacije( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "nacin_placanja VARCHAR(255) NOT NULL, " +
                "id_gost INT NOT NULL, " +
                "id_sobe INT NOT NULL, " +
                "PRIMARY KEY(id), " +
                "FOREIGN KEY (id_gost) REFERENCES gosti(id), " +
                "FOREIGN KEY (id_sobe) REFERENCES sobe(id));";
            Console.WriteLine(runQuery(query));
        }

        private static void insertData() {
            //implementacija metode
            string query = "insert into gosti values(0,'Pera','Peric',18,'Visa');" +
                            "insert into gosti values(0, 'Djura', 'Djuric', 25, 'Visa');" +
                            "insert into gosti values(0, 'Marko', 'Markovic', 45, 'Master');" +
                            "insert into gosti values(0, 'Milica', 'Miric', 29, 'Master');" +
                            "insert into gosti values(0, 'Marija', 'Milicevic', 35, 'AE');" +
                            "insert into gosti values(0, 'Milos', 'Milovic', 34, 'AE');" +


                            "insert into sobe values(0, 'Jednokrevetna', 45, 1, 2);" +
                            "insert into sobe values(0, 'Dvokrevetna', 60, 2, 3);" +
                            "insert into sobe values(0, 'Apartman', 90, 4, 6);" +

                            "insert into rezervacije values(0, 'Kes', 2, 1);" +
                            "insert into rezervacije values(0, 'Kes', 3, 2);" +
                            "insert into rezervacije values(0, 'Kes', 6, 3);" +
                            "insert into rezervacije values(0, 'Kreditna', 2, 3);" +
                            "insert into rezervacije values(0, 'Kreditna', 4, 3);" +
                            "insert into rezervacije values(0, 'Kreditna', 1, 1);" +
                            "insert into rezervacije values(0, 'Kes', 5, 2);";
            Console.WriteLine(runQuery(query));
        }

        private static void executeQueries() {
            string[] niz = {"SELECT g.ime, g.prezime FROM gosti as g INNER JOIN sobe as s ON g.id = s.id WHERE" +
                            " s.vrsta_sobe = \'jednokrevetna\'",
                        "SELECT * FROM sobe WHERE cena = (SELECT MAX(cena) FROM sobe)",
                        "SELECT g.ime, g.prezime, r.nacin_placanja FROM rezervacije as r INNER JOIN gosti " +
                            "as g ON r.id_gost = g.id WHERE r.nacin_placanja = \"kreditna\"",
                        "SELECT COUNT(*) FROM rezervacije as r INNER JOIN sobe as s ON r.id_sobe = s.id WH" +
                            "ERE r.nacin_placanja = \"Kes\" AND s.vrsta_sobe = \"Dvokrevetna\"",
                        "SELECT AVG(s.broj_lezaja) FROM rezervacije as r INNER JOIN gosti as g ON r.id_gos" +
                            "t = g.id INNER JOIN sobe as s ON r.id_sobe = s.id WHERE g.dob > 30",
                        "SELECT id_sobe, COUNT(*) FROM rezervacije GROUP BY id_sobe ORDER BY COUNT(*) DESC" +
                            " LIMIT 1",
                        "SELECT id_sobe, COUNT(*) FROM rezervacije GROUP BY id_sobe HAVING count(*) >= ALL" +
                        " (SELECT COUNT(*) FROM rezervacije GROUP by id_sobe)"};
            while (true)
            {
                Console.WriteLine("Select one of queries or quit: ");
                for (int i = 0; i < niz.Length; i++)
                {
                    Console.WriteLine(i + ". " + niz[i]);
                }
                Console.WriteLine(niz.Length + ". QUIT");
                int query_num = Convert.ToInt32(Console.ReadLine());
                if (query_num >= 0 && query_num < niz.Length)
                {
                    string text = "";
                    string[] rezultat = runQuery(niz[query_num]).Split('|');
                    foreach (string rez in rezultat)
                    {
                        text += rez;
                    }
                    Console.WriteLine(text);
                }
                else if (query_num == niz.Length) {
                    Console.WriteLine("\n");
                    break;
                }
            }
        }

        //metod za izvrsavanje upita
        private static string runQuery(string query)
        {

            if (query == "")
            {
                return ("No query inserted!");
            }
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=hotel";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {

                databaseConnection.Open();

                if (!(query.Split(' ')[0].ToLower().Equals("select")))
                {
                    int number = commandDatabase.ExecuteNonQuery();
                    Console.WriteLine(number + " redova je promenjeno!");
                    return query;
                }


                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    Console.WriteLine("Your query generated results");
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("|\n");
                    }
                    //Console.WriteLine(rez);
                    return rez.ToString();
                }
                else
                {
                    Console.WriteLine("Success");
                }

                databaseConnection.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Error" + e.Message);
            }
            return "";
        }
    }
}
