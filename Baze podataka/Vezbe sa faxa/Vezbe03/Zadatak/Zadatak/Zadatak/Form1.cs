﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Zadatak
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string query = "CREATE TABLE gosti( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "prezime VARCHAR(255) NOT NULL, " +
                "dob INT NOT NULL, " +
                "kreditna_kartica VARCHAR(255) NOT NULL, " +
                "PRIMARY KEY(id));" +

                "CREATE TABLE sobe( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "vrsta_sobe VARCHAR(255) NOT NULL, " +
                "cena INT NOT NULL, " +
                "broj_lezaja INT NOT NULL, " +
                "gost_id INT, " +
                "PRIMARY KEY(id), " +
                "FOREIGN KEY (gost_id) REFERENCES gosti(id));" +

                "CREATE TABLE rezervacije( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "nacin_placanja VARCHAR(255) NOT NULL, " +
                "id_gost INT NOT NULL, " +
                "id_sobe INT NOT NULL, " +
                "PRIMARY KEY(id), " +
                "FOREIGN KEY (id_gost) REFERENCES gosti(id), " +
                "FOREIGN KEY (id_sobe) REFERENCES sobe(id));";
            MessageBox.Show(runQuery(query));
        }

        //metod za izvrsavanje upita
        private string runQuery(string query)
        {

            if (query == "")
            {
                return ("No query inserted!");
            }
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=student;password=student;database=hotel";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {

                databaseConnection.Open();

                if (!(query.Split(' ')[0].ToLower().Equals("select")))
                {
                    int number = commandDatabase.ExecuteNonQuery();
                    MessageBox.Show(number + " redova je promenjeno!");
                    return query;
                }


                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    MessageBox.Show("Your query generated results");
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("|\n");
                    }
                    Console.WriteLine(rez);
                    return rez.ToString();
                }
                else
                {
                    MessageBox.Show("Success");
                }

                databaseConnection.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "";
        }

    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string query = "insert into gosti values(0,'Pera','Peric',18,'Visa');" +
                            "insert into gosti values(0, 'Djura', 'Djuric', 25, 'Visa');" +
                            "insert into gosti values(0, 'Marko', 'Markovic', 45, 'Master');" +
                            "insert into gosti values(0, 'Milica', 'Miric', 29, 'Master');" +
                            "insert into gosti values(0, 'Marija', 'Milicevic', 35, 'AE');" +
                            "insert into gosti values(0, 'Milos', 'Milovic', 34, 'AE');" +


                            "insert into sobe values(0, 'Jednokrevetna', 45, 1, 2);" +
                            "insert into sobe values(0, 'Dvokrevetna', 60, 2, 3);" +
                            "insert into sobe values(0, 'Apartman', 90, 4, 6);" +

                            "insert into rezervacije values(0, 'Kes', 2, 1);" +
                            "insert into rezervacije values(0, 'Kes', 3, 2);" +
                            "insert into rezervacije values(0, 'Kes', 6, 3);" +
                            "insert into rezervacije values(0, 'Kreditna', 2, 3);" +
                            "insert into rezervacije values(0, 'Kreditna', 4, 3);" +
                            "insert into rezervacije values(0, 'Kreditna', 1, 1);" +
                            "insert into rezervacije values(0, 'Kes', 5, 2);";
            MessageBox.Show(runQuery(query));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            string query = listBox1.Items[listBox1.SelectedIndex].ToString();
            string[] rezultat = runQuery(query).Split('|');
            foreach (string rez in rezultat) {
                textBox2.AppendText(rez + Environment.NewLine);
            }

        }
    }
}
