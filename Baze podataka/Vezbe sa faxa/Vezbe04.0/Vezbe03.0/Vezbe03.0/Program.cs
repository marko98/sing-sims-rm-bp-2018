﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Vezbe03._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Do you want to create tables: (Y/N)");
            string answer_base = Console.ReadLine();
            if (answer_base.ToLower() == "y")
            {
                createTables();
            }

            Console.WriteLine("Do you want to insert data: (Y/N)");
            string fill_base = Console.ReadLine();
            if (fill_base.ToLower() == "y")
            {
                insertData();
            }

            Console.WriteLine("Do you want to execute some queries: (Y/N)");
            string execute_queries = Console.ReadLine();
            if (execute_queries.ToLower() == "y")
            {
                executeQueries();
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        private static void createTables()
        {
            //implementacija metode
            string query = "CREATE TABLE kupci( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "grad VARCHAR(255) NOT NULL, " +
                "PRIMARY KEY(id));" +

                "CREATE TABLE artikli( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "cena INT NOT NULL, " +
                "PRIMARY KEY(id));" +

                "CREATE TABLE prodavnice( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "grad VARCHAR(255) NOT NULL, " +
                "PRIMARY KEY(id)); " +

                "CREATE TABLE porudzbine( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "id_kupca INT NOT NULL, " +
                "id_artikla INT NOT NULL, " +
                "id_prodavnice INT NOT NULL, " +
                "kolicina INT NOT NULL, " +
                "datum DATE NOT NULL, " +
                "PRIMARY KEY(id), " +
                "FOREIGN KEY (id_kupca) REFERENCES kupci(id), " +
                "FOREIGN KEY (id_prodavnice) REFERENCES prodavnice(id), " +
                "FOREIGN KEY (id_artikla) REFERENCES artikli(id));";
            Console.WriteLine(runQuery(query));
        }

        private static void insertData()
        {
            //implementacija metode
            string query = "insert into kupci values(0, 'Petar', 'Cacak');" +
                            "insert into kupci values(0, 'Marko', 'Beograd');" +
                            "insert into kupci values(0, 'Ivan', 'Cacak');" +
                            "insert into kupci values(0, 'Dusan', 'Beograd');" +
                            "insert into kupci values(0, 'Jelena', 'Novi Sad');" +
                            "insert into kupci values(0, 'Ana', 'Novi Sad');" +
                            "insert into kupci values(0, 'Stefan', 'Novi Sad');" +


                            "insert into artikli values(0, 'hleb', 50);" +
                            "insert into artikli values(0, 'mleko', 100);" +
                            "insert into artikli values(0, 'jaja', 5);" +
                            "insert into artikli values(0, 'sok', 200);" +
                            "insert into artikli values(0, 'voda', 50);" +

                            "insert into prodavnice values(0, 'Maxi-Ca', 'Cacak');" +
                            "insert into prodavnice values(0, 'Idea-Bg', 'Beograd');" +
                            "insert into prodavnice values(0, 'Merkator', 'Beograd');" +
                            "insert into prodavnice values(0, 'Maxi-NS', 'Novi Sad');" +
                            "insert into prodavnice values(0, 'Idea-NS', 'Novi Sad');" +

                            "insert into porudzbine values(0, 1, 1, 1, 5, '2016-1-1');" +
                            "insert into porudzbine values(0, 2, 1, 2, 2, '2017-2-1');" +
                            "insert into porudzbine values(0, 2, 1, 3, 3, '2017-2-1');" +
                            "insert into porudzbine values(0, 3, 3, 4, 1, '2018-6-1');" +
                            "insert into porudzbine values(0, 3, 4, 5, 1, '2015-7-1');" +
                            "insert into porudzbine values(0, 3, 5, 1, 2, '2016-9-1');" +
                            "insert into porudzbine values(0, 4, 3, 3, 10, '2016-2-5');" +
                            "insert into porudzbine values(0, 4, 5, 4, 2, '2018-6-1');" +
                            "insert into porudzbine values(0, 5, 1, 5, 4, '2017-4-3');" +
                            "insert into porudzbine values(0, 7, 3, 4, 7, '2016-9-1');" +
                            "insert into porudzbine values(0, 7, 5, 2, 2, '2015-7-1');";
            Console.WriteLine(runQuery(query));
        }

        private static void executeQueries()
        {
            string[] niz = {"SELECT kupci.ime, COUNT(*) FROM porudzbine INNER JOIN kupci on porudzbine.id_kupca = kupci.id GROUP BY id_kupca HAVING COUNT(*) >= ALL(SELECT COUNT(*) FROM porudzbine GROUP BY id_kupca) ORDER BY COUNT(*) DESC",
                            "SELECT kupci.ime, COUNT(*) FROM porudzbine INNER JOIN kupci on porudzbine.id_kupca = kupci.id GROUP BY id_kupca ORDER BY COUNT(*) DESC LIMIT 1",
                            "SELECT artikli.ime, SUM(kolicina) FROM porudzbine INNER JOIN prodavnice on prodavnice.id = id_prodavnice INNER JOIN artikli on artikli.id = id_artikla WHERE prodavnice.grad = 'Beograd' GROUP BY id_artikla ORDER BY SUM(kolicina) DESC LIMIT 1",
                            "SELECT kupci.ime, kolicina*artikli.cena FROM porudzbine INNER JOIN artikli on artikli.id = porudzbine.id_artikla INNER JOIN kupci on kupci.id = porudzbine.id_kupca",
                            "SELECT prodavnice.ime, SUM(artikli.cena * porudzbine.kolicina) FROM porudzbine  INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice INNER JOIN artikli on artikli.id = porudzbine.id_artikla GROUP BY prodavnice.ime",
                            "SELECT *  FROM porudzbine INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice WHERE prodavnice.grad = 'Novi Sad'",
                            "SELECT prodavnice.grad, COUNT(*) FROM porudzbine INNER JOIN prodavnice on prodavnice.id = porudzbine.id_prodavnice GROUP BY prodavnice.grad",
                            "SELECT kupci.ime, AVG(porudzbine.kolicina*artikli.cena) FROM porudzbine INNER JOIN artikli ON artikli.id = porudzbine.id_artikla INNER JOIN kupci ON kupci.id = porudzbine.id_kupca GROUP BY kupci.ime",
                            "SELECT porudzbine.datum, COUNT(*) FROM porudzbine GROUP BY porudzbine.datum",
                            "SELECT * FROM porudzbine INNER JOIN artikli on artikli.id = porudzbine.id_artikla ORDER BY (porudzbine.kolicina*artikli.cena) DESC LIMIT 1"};
            while (true)
            {
                Console.WriteLine("Select one of queries or quit: ");
                for (int i = 0; i < niz.Length; i++)
                {
                    Console.WriteLine(i + ". " + niz[i]);
                }
                Console.WriteLine(niz.Length + ". QUIT");
                int query_num = Convert.ToInt32(Console.ReadLine());
                if (query_num >= 0 && query_num < niz.Length)
                {
                    string text = "";
                    string[] rezultat = runQuery(niz[query_num]).Split('|');
                    foreach (string rez in rezultat)
                    {
                        text += rez;
                    }
                    Console.WriteLine(text);
                }
                else if (query_num == niz.Length)
                {
                    Console.WriteLine("\n");
                    break;
                }
            }
        }

        //metod za izvrsavanje upita
        private static string runQuery(string query)
        {

            if (query == "")
            {
                return ("No query inserted!");
            }
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=student;password=student;database=priprema_za_kolokvijum01";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {

                databaseConnection.Open();

                if (!(query.Split(' ')[0].ToLower().Equals("select")))
                {
                    int number = commandDatabase.ExecuteNonQuery();
                    Console.WriteLine(number + " redova je promenjeno!");
                    return query;
                }


                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    Console.WriteLine("Your query generated results");
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("|\n");
                    }
                    //Console.WriteLine(rez);
                    return rez.ToString();
                }
                else
                {
                    Console.WriteLine("Success");
                }

                databaseConnection.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Error" + e.Message);
            }
            return "";
        }
    }
}
