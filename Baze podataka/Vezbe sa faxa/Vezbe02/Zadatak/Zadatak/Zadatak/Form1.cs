﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Zadatak
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string query = "CREATE TABLE kupci( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "prezime VARCHAR(255) NOT NULL, " +
                "grad VARCHAR(255) NOT NULL, " +
                "datum_rodjenja DATE NOT NULL, " +
                "dug DECIMAL NOT NULL, " +
                "PRIMARY KEY(id))";
            MessageBox.Show(runQuery(query));
        }

        //metod za izvrsavanje upita
        private string runQuery(string query)
        {

            if (query == "")
            {
                return ("No query inserted!");
            }
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=student;password=student;database=moja_baza_vezbe02";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {

                databaseConnection.Open();

                if (!(query.Split(' ')[0].ToLower().Equals("select")))
                {
                    int number = commandDatabase.ExecuteNonQuery();
                    MessageBox.Show(number + " redova je promenjeno!");
                    return query;
                }


                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    MessageBox.Show("Your query generated results");
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("|\n");
                    }
                    Console.WriteLine(rez);
                    return rez.ToString();
                }
                else
                {
                    MessageBox.Show("Success");
                }

                databaseConnection.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "";
        }

    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string query = "INSERT INTO kupci (ime, prezime, grad, datum_rodjenja, dug) VALUES ('Marko', 'Markovic', 'Cacak', '1995-2-3', 1000);" +
                "INSERT INTO kupci (ime, prezime, grad, datum_rodjenja, dug) VALUES ('Ivan', 'Ivanovic', 'Beograd', '1990-4-4', 2500);" +
                "INSERT INTO kupci (ime, prezime, grad, datum_rodjenja, dug) VALUES ('Nikola', 'Nikolic', 'Novi Sad', '2001-1-1', 5000);" +
                "INSERT INTO kupci (ime, prezime, grad, datum_rodjenja, dug) VALUES ('Jelena', 'Jelenic', 'Novi Sad', '1998-5-5', 10000);" +
                "INSERT INTO kupci (ime, prezime, grad, datum_rodjenja, dug) VALUES ('Milica', 'Ivic', 'Beograd', '2000-1-5', 1500);";
            MessageBox.Show(runQuery(query));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            string query = listBox1.Items[listBox1.SelectedIndex].ToString();
            string[] rezultat = runQuery(query).Split('|');
            foreach (string rez in rezultat) {
                textBox2.AppendText(rez + Environment.NewLine);
            }

        }
    }
}
