﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//dodajemo ovu referencu
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string rezultat = runQuery();
            MessageBox.Show(rezultat);

        }
        //metod za izvrsavanje upita
        private string runQuery()
        {
            //definisemo text iz textBoxa kao query
            string query = textBox1.Text;

            if(query == "")
            {
                return ("No query inserted!");
            }
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=univerzitet";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {
                
                databaseConnection.Open();

                if (!(query.Split(' ')[0].ToLower().Equals("select"))) {
                    int number = commandDatabase.ExecuteNonQuery();
                    MessageBox.Show(number + " redova je promenjeno!");
                    return query;
                }


                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    MessageBox.Show("Your query generated results");
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("\n");
                    }
                    return rez.ToString();
                }
                else
                {
                    MessageBox.Show("Success");
                }

                databaseConnection.Close();

            }
            catch(Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
