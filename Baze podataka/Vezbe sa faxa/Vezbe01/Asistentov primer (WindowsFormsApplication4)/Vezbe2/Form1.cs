﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Vezbe2
{
    public partial class Form1 : Form
    {

        private int last_professor_id;

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string professor_name = textBox1.Text;

            if (professor_name == "")
            {
                MessageBox.Show("Niste uneli ime profesora!");
                return;
            }

            string query = "select * from profesori where ime='" + professor_name +"';";

            string professor_info = runQuery(query);

            if (professor_info == "")
            {
                MessageBox.Show("Ne postoji profesor sa tim imenom!");
            }
            else
            {
                textBox2.Text = professor_info;
                button2.Enabled = true;
            }
        }

        //metod za izvrsavanje upita, kao parametar prima tekst upita, a kao rezultat vraca string sa rezultatima upita
        //ili prazan string ako nema rezultata
        private string runQuery(string query)
        {
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=vezbe";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {

                databaseConnection.Open();

                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        last_professor_id = int.Parse(myReader.GetString(0));
                        //ova for petlja samo formatira informaciju o profesoru dodavajuci razmak izmedju kolona
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("\n");
                    }
                    return rez.ToString();
                }

                databaseConnection.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Greska: " + e.Message);           
            }
            return "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string query = "select * from predmeti where profesor=" + last_professor_id;

            string professor_subjects = runQuery(query);

            string file_name = "subjects_" + last_professor_id + ".txt";

            string working_directory = AppDomain.CurrentDomain.BaseDirectory;
           
            string project_root_directory = Directory.GetParent(working_directory).Parent.Parent.FullName;

            File.WriteAllText(project_root_directory + @"\" + file_name, professor_subjects);

            MessageBox.Show("Predmeti profesora su sacuvani u fajl: " + file_name);

            button2.Enabled = false;
        }
    }
}
