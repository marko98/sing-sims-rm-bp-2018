﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button2.Enabled = true;
            runQuery2();
        }

        private string runQuery()
        {
            string ime = textBox1.Text;

            if (ime == "")
            {
                MessageBox.Show("Niste uneli ime.");
                return "";
            }
            string query = "SELECT * FROM `profesori` WHERE ime LIKE \"%" + ime + "%\"";

            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=univerzitet";

            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();

                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    MessageBox.Show("Your query generated results");

                    StringBuilder rez = new StringBuilder();
                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                            //Console.WriteLine("Line ->" + myReader.GetString(i) + "<-");
                        }
                        rez.Append("\n");
                    }
                    return rez.ToString();
                }
                else
                {
                    MessageBox.Show("Success");
                    return "";
                }
                databaseConnection.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
                return "";
            }
        }

        private void runQuery2()
        {
            textBox2.Text = "";
            string ime = textBox1.Text;

            if (ime == "")
            {
                MessageBox.Show("Niste uneli ime.");
                return;
            }
            string query = "SELECT * FROM `profesori` WHERE ime LIKE \"%" + ime + "%\"";

            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=univerzitet";

            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();

                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    MessageBox.Show("Your query generated results");

                    StringBuilder rez = new StringBuilder();
                    while (myReader.Read())
                    {
                        rez.Clear();
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                            //Console.WriteLine("Line ->" + myReader.GetString(i) + "<-");
                        }
                        textBox2.AppendText(rez.ToString());
                        textBox2.AppendText(Environment.NewLine);
                        //Console.WriteLine(rez);
                    }
                    //Console.WriteLine(rez);
                }
                else
                {
                    MessageBox.Show("Success");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
                databaseConnection.Close();
            }

            databaseConnection.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string podaci = textBox2.Text;
            //Console.WriteLine(podaci);
            podaci = podaci.Replace("\n", ";");
            //Console.WriteLine(podaci);
            string[] niz = podaci.Split(';');
            for (int i = 0; i < niz.Length; i++) {
                if (niz[i].Length > 0) {
                    //Console.WriteLine("|" + niz[i][0] + "|");
                    writeDown(niz[i][0]);
                }
            }
        }

        private void writeDown(char id_profesora) {
            string professor_subjects = "";

            string query = "SELECT * FROM `predmeti` WHERE id_profesora = " + id_profesora;

            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=univerzitet";

            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();

                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    //MessageBox.Show("Your query generated results");

                    StringBuilder rez = new StringBuilder();
                    while (myReader.Read())
                    {
                        rez.Clear();
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                            //Console.WriteLine("Line ->" + myReader.GetString(i) + "<-");
                        }
                        professor_subjects = professor_subjects + rez + "\n";
                        //Console.WriteLine("|" + rez + "|");
                    }
                    //Console.WriteLine(professor_subjects);
                }
                else
                {
                    MessageBox.Show("Success");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
                databaseConnection.Close();
            }

            databaseConnection.Close();


            string working_directory = AppDomain.CurrentDomain.BaseDirectory;

            string project_root_directory = Directory.GetParent(working_directory).Parent.Parent.FullName;

            //Console.WriteLine(project_root_directory);

            string file_name = "subjects_" + id_profesora +".txt";

            File.WriteAllText(project_root_directory + @"\" + file_name, professor_subjects);

            MessageBox.Show("Predmeti profesora su sacuvani u fajl: " + file_name);

            professor_subjects = "";

            button2.Enabled = false;
        }
    }
}


