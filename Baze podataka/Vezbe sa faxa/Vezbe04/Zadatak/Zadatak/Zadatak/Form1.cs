﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Zadatak
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string query = "CREATE TABLE kupci( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "grad VARCHAR(255) NOT NULL, " +
                "PRIMARY KEY(id));" +

                "CREATE TABLE artikli( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "cena INT NOT NULL, " +
                "PRIMARY KEY(id));" +

                "CREATE TABLE prodavnice( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "ime VARCHAR(255) NOT NULL, " +
                "grad VARCHAR(255) NOT NULL, " +
                "PRIMARY KEY(id)); " +

                "CREATE TABLE porudzbine( " +
                "id INT NOT NULL AUTO_INCREMENT, " +
                "id_kupca INT NOT NULL, " +
                "id_artikla INT NOT NULL, " +
                "id_prodavnice INT NOT NULL, " +
                "kolicina INT NOT NULL, " +
                "datum DATE NOT NULL, " +
                "PRIMARY KEY(id), " +
                "FOREIGN KEY (id_kupca) REFERENCES kupci(id), " +
                "FOREIGN KEY (id_prodavnice) REFERENCES prodavnice(id), " +
                "FOREIGN KEY (id_artikla) REFERENCES artikli(id));";
            MessageBox.Show(runQuery(query));
        }

        //metod za izvrsavanje upita
        private string runQuery(string query)
        {

            if (query == "")
            {
                return ("No query inserted!");
            }
            //defaultni string za konekciju sa bazom
            //datasource je na koji IP se kacimo (u nasem slucaju localhost ili 127.0.0.1) ; default port na XAMPP-u je 3306; username i pass su od naloga na racunaru; database je baza na koju se kacimo
            string MySqlConnectionString = "datasource=localhost;port=3306;username=student;password=student;database=priprema_za_kolokvijum01";
            //kreiramo novu promenljivu konekcije
            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            //kreiramo novu promenljivu za cuvanje upita
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            try
            {

                databaseConnection.Open();

                if (!(query.Split(' ')[0].ToLower().Equals("select")))
                {
                    int number = commandDatabase.ExecuteNonQuery();
                    MessageBox.Show(number + " redova je promenjeno!");
                    return query;
                }


                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    MessageBox.Show("Your query generated results");
                    StringBuilder rez = new StringBuilder();

                    while (myReader.Read())
                    {
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            rez.Append(myReader.GetString(i));
                            rez.Append(" ");
                        }
                        rez.Append("|\n");
                    }
                    Console.WriteLine(rez);
                    return rez.ToString();
                }
                else
                {
                    MessageBox.Show("Success");
                }

                databaseConnection.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "";
        }

    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //implementacija metode
            string query = "insert into kupci values(0, 'Petar', 'Cacak');" +
                            "insert into kupci values(0, 'Marko', 'Beograd');" +
                            "insert into kupci values(0, 'Ivan', 'Cacak');" +
                            "insert into kupci values(0, 'Dusan', 'Beograd');" +
                            "insert into kupci values(0, 'Jelena', 'Novi Sad');" +
                            "insert into kupci values(0, 'Ana', 'Novi Sad');" +
                            "insert into kupci values(0, 'Stefan', 'Novi Sad');" +


                            "insert into artikli values(0, 'hleb', 50);" +
                            "insert into artikli values(0, 'mleko', 100);" +
                            "insert into artikli values(0, 'jaja', 5);" +
                            "insert into artikli values(0, 'sok', 200);" +
                            "insert into artikli values(0, 'voda', 50);" +

                            "insert into prodavnice values(0, 'Maxi-Ca', 'Cacak');" +
                            "insert into prodavnice values(0, 'Idea-Bg', 'Beograd');" +
                            "insert into prodavnice values(0, 'Merkator', 'Beograd');" +
                            "insert into prodavnice values(0, 'Maxi-NS', 'Novi Sad');" +
                            "insert into prodavnice values(0, 'Idea-NS', 'Novi Sad');" +

                            "insert into porudzbine values(0, 1, 1, 1, 5, '2016-1-1');" +
                            "insert into porudzbine values(0, 2, 1, 2, 2, '2017-2-1');" +
                            "insert into porudzbine values(0, 2, 1, 3, 3, '2017-2-1');" +
                            "insert into porudzbine values(0, 3, 3, 4, 1, '2018-6-1');" +
                            "insert into porudzbine values(0, 3, 4, 5, 1, '2015-7-1');" +
                            "insert into porudzbine values(0, 3, 5, 1, 2, '2016-9-1');" +
                            "insert into porudzbine values(0, 4, 3, 3, 10, '2016-2-5');" +
                            "insert into porudzbine values(0, 4, 5, 4, 2, '2018-6-1');" +
                            "insert into porudzbine values(0, 5, 1, 5, 4, '2017-4-3');" +
                            "insert into porudzbine values(0, 7, 3, 4, 7, '2016-9-1');" +
                            "insert into porudzbine values(0, 7, 5, 2, 2, '2015-7-1');";
            MessageBox.Show(runQuery(query));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            try
            {
                string query = listBox1.Items[listBox1.SelectedIndex].ToString();
                string[] rezultat = runQuery(query).Split('|');
                foreach (string rez in rezultat)
                {
                    textBox2.AppendText(rez + Environment.NewLine);
                }
            }
            catch (Exception a) {
                Console.WriteLine("Odaberi upit.");
            }
            
        }
    }
}
